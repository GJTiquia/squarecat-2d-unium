# 9Cat-2d-Unium

Table of Contents

1. [Commands](#commands)
1. [File Structure](#file-structure)
1. [Test-Driven Development](#test-driven-development)

## Commands

Make sure the Unity Editor or Development Build is running before running the commands below.

Troubleshooting:

- If cannot connect, first see if can connect via the browser. Make sure the port is correct.
- If cannot connect, check to see if connecting to WiFi can fix the problem.

### Sandbox Code

Run sandbox code in `src/index.ts`

```bash
npm start
```

### Run All Tests

Run all tests sequentially in alphabetical order

```bash
npm run test
```

### Run Specific Tests

Run tests in a specific folder sequentially in alphabetical order, or a specific file

```bash
npm run test -- <test-name>
```

The `test-name` can be the full name of a folder like `AOO_ConnectionTests`, the short form `A00`, or even a specific file like `A03`

You can add run multiple test folders/files in sequence by adding a space between `test-name`.

### Run Development Tests

Run a specific test including the automated set up. Typically used by game devs during development while adding new features that need the same setup.

The current setup is EnterSocialHub from Login. All other tests start from the SocialHub.

```bash
npm run test:dev -- <test-name>
```

Under the hood, this runs test `A00` and `B00` before running the desired test, a short form of `npm run test -- A00 B00 <test-name>`

### Run All Tests With No Setup

Runs all the tests except `A00` and `B00`

```bash
npm run test:no-setup
```

## File Structure

### File Naming

Tests run in alphabetical order in sequence, but flexibility also needs to be given for the file naming for easier readability.

Therefore each file and folder is named with a 3-digit code followed by a short descriptive name. With the exception of [DEV Tests](#dev-tests).

The first digit is a letter, followed by two numbers.

Folders should be marked as `_00`, and the files in the folder should start from `_01` onwards.

This is so that by running a command such as `npm run test -- A00`, it can be easily understood as running all the test in the folder `A00`. The command `npm run test -- A03` will then be understood as running a single test file `A03`.

### Reserved File Names

`A00` is reserved for testing the connection to the running game and for testing the internal tools if they are functioning properly, eg. the `UniumWebSocket` class and its methods.

`B00` is reserved for setting up the game to a state ready to be tested. Currently `B00` logs in the game and enters the SocialHub.

`DEV00` is reserved for [DEV tests](#dev-tests).

### Test File Independence

Tests from `C00` onwards can be assumed to start after `A00` and `B00`.

Tests from `C00` onwards should be able to run independently from each other. They should all have the same starting point and ending point, such that running `A00 B00 C00 D00 E01` is the same as running `A00 B00 C00 E01` or `A00 B00 E01`. The starting point of each test file should be from `B00` (ie. in SocialHub), the ending point of each test file should be returning to the same state as how it started at `B00` (ie. back in Social Hub).

With this in mind, tests in the future can be potentially run in parallel.

## Test-Driven Development

### DEV Tests

DEV tests are the tests in the `DEV00` folder. This folder is reserved for use during development of features.

The test files in this folder is meant for quickly writing test scripts while in the creative development flow without needing to think about organizing the tests.

When a feature has been implemented successfully, the test files should be renamed and organized properly.

The test files in here should follow the same rules as listed in [Test File Independence](#test-file-independence). This way, files can be freely reorganized into other folders.

The file name in this folder are not important, but typically one would follow a structure `D01_someDescriptiveName.test.ts`. When multiple developers are working on multiple features at the same time, it is inevitable that the code will be the same anyways. The descriptive name would most likely be different, so the possiblilty of having conflicts is also quite low. The test files should be reorganized anyways later on.
