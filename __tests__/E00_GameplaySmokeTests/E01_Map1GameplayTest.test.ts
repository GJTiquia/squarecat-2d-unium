import { UniumWebSocket } from "../../src/UniumWebSocket";
import { SceneName } from "../../src/enums";
import { GameObject, Vector3 } from "../../src/types";
import { sleep } from "../../src/utils";

describe("Map 1 Gameplay Test", () => {
    let u: UniumWebSocket;

    beforeAll(async () => {
        u = new UniumWebSocket();
        await u.connectAsync();
    })

    afterAll(async () => {
        await u.disconnectAsync();
    })

    it("should be in SocialHubScene", async () => {
        const about = await u.getAboutAsync();
        expect(about.Scene).toEqual(SceneName.SocialHubScene);
    })


    it("should load into GameScene successfully", async () => {
        expect.assertions(1);

        const teleportPosition: Vector3 = await u.getFirstResultAsync("/q/scene//Map1TriggerTeleportPoint.Transform.position")
        await u.getAsync(`/q/scene//Unium.UniumHelpers.TeleportLocalPlayer(${JSON.stringify(teleportPosition)})`);
        await sleep(500);
        await u.getAsync("/q/scene//Unium.UniumHelpers.Interact()");

        const sceneLoaded = await u.waitForScene(SceneName.GameScene);
        expect(sceneLoaded).toBeTruthy();
    });

    it("should have the map name as Map 1", async () => {
        const mapName: string = await u.getFirstResultAsync("/q/scene//Unium.UniumHelpers.GetCurrentMap()")
        expect(mapName).toEqual("Map 1");
    })

    it("should have the exit portal active when all collectables are collected", async () => {
        const positions = await u.getAsync("/q/scene//Collectable*.Transform.position") as Array<Vector3>

        for (let i = 0; i < positions.length; i++) {
            const teleportPosition: Vector3 = positions[i];

            await u.getAsync(`/q/scene//Unium.UniumHelpers.TeleportLocalPlayer(${JSON.stringify(teleportPosition)})`);
            await sleep(500);
        }

        const exitPortal: GameObject = await u.getFirstResultAsync("/q/scene//ExitPortal");
        expect(exitPortal.activeInHierarchy).toBeTruthy();
    });


    it("should return to social hub when the player touches the exit portal", async () => {
        const teleportPosition: Vector3 = await u.getFirstResultAsync("/q/scene//ExitPortal.Transform.position");

        await sleep(500);
        await u.getAsync(`/q/scene//Unium.UniumHelpers.TeleportLocalPlayer(${JSON.stringify(teleportPosition)})`);

        const sceneLoaded = await u.waitForScene(SceneName.SocialHubScene);
        expect(sceneLoaded).toBeTruthy();
    });
})