import { UniumWebSocket } from "../../src/UniumWebSocket";
import { SceneName } from "../../src/enums";
import { Vector3 } from "../../src/types";
import { sleep } from "../../src/utils";
import { expectVector3ToBeClose } from "../../src/utils/testUtils";

describe("Load Map 2 Test", () => {
    let u: UniumWebSocket;

    beforeAll(async () => {
        u = new UniumWebSocket();
        await u.connectAsync();
    })

    afterAll(async () => {
        await u.disconnectAsync();
    })

    it("should be in SocialHubScene", async () => {
        const about = await u.getAboutAsync();
        expect(about.Scene).toEqual(SceneName.SocialHubScene);
    })

    it("should teleport near the map 2 trigger", async () => {
        const teleportPosition: Vector3 = await u.getFirstResultAsync("/q/scene//Map2TriggerTeleportPoint.Transform.position")
        await u.getAsync(`/q/scene//Unium.UniumHelpers.TeleportLocalPlayer(${JSON.stringify(teleportPosition)})`);

        const teleportedPosition: Vector3 = await u.getFirstResultAsync("/q/scene//Unium.UniumHelpers.GetLocalPlayerPosition()")
        expectVector3ToBeClose(teleportPosition, teleportedPosition);
    })

    it("should change to GameScene when the player presses interact", async () => {
        expect.assertions(1);

        await sleep(500);
        await u.getAsync("/q/scene//Unium.UniumHelpers.Interact()");

        const sceneLoaded = await u.waitForScene(SceneName.GameScene);
        expect(sceneLoaded).toBeTruthy();
    })

    it("should have the map name as Map 2", async () => {
        const mapName: string = await u.getFirstResultAsync("/q/scene//Unium.UniumHelpers.GetCurrentMap()")
        expect(mapName).toEqual("Map 2");
    })

    // TODO : Should be replaced with pause menu and return to social hub
    it("should return to SocialHub when ExitPortal is called", async () => {
        expect.assertions(1);

        await sleep(500);
        await u.getAsync("/q/scene//ExitPortal.ExitPortal.ReturnToSocialHub()");

        const sceneLoaded = await u.waitForScene(SceneName.SocialHubScene);
        expect(sceneLoaded).toBeTruthy();
    })
})