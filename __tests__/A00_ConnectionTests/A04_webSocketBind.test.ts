import { UniumWebSocket } from "../../src/UniumWebSocket";

describe("WebSocket Bind Test", () => {
    let u: UniumWebSocket;

    beforeAll(async () => {
        u = new UniumWebSocket();
        await u.connectAsync();
    })

    afterAll(async () => {
        await u.disconnectAsync();
    })

    it("should bind to debug event successfully", async () => {
        const response = await u.bindAsync("/events.debug", "debugEventID");
        expect(response.id).toEqual("debugEventID");
        expect(response.info).toEqual("bound");
    })

    it("should listen to debug event successfully", async () => {
        u.getAsync("/q//Unium.UniumHelpers.InvokeDebugLog()");

        const response = await u.waitForEventAsync("debugEventID", 3000);

        expect(response.id).toEqual("debugEventID");
        expect(response.data).toBeDefined();
        expect(response.data.message).toEqual("UniumHelpers.InvokeDebugLog");
        expect(response.data.type).toEqual("Log");
    })

    it("should unbind debug event successfully", async () => {
        const response = await u.unbindAsync("debugEventID");
        expect(response.id).toEqual("debugEventID");
        expect(response.info).toEqual("unbound");
    })

    it("should bind to a void event successfully", async () => {
        const response = await u.bindAsync("/scene/DeveloperTools/Unium.UniumHelpers.TestVoidEvent", "TestVoidEvent");
        expect(response.id).toEqual("TestVoidEvent");
        expect(response.info).toEqual("bound");
    });

    it("should listen to a binded void event successfully", async () => {
        u.getAsync("/q//Unium.UniumHelpers.InvokeTestVoidEvent()");

        const response = await u.waitForEventAsync("TestVoidEvent", 3000);
        expect(response.id).toEqual("TestVoidEvent");
        expect(response.data).toBeFalsy(); // Eg. if it is null
    });

    it("should unbind test void event successfully", async () => {
        const response = await u.unbindAsync("TestVoidEvent");
        expect(response.id).toEqual("TestVoidEvent");
        expect(response.info).toEqual("unbound");
    })

    it("should bind to an int event successfully", async () => {
        const response = await u.bindAsync("/scene/DeveloperTools/Unium.UniumHelpers.TestIntEvent", "TestIntEvent");
        expect(response.id).toEqual("TestIntEvent");
        expect(response.info).toEqual("bound");
    });


    it("should listen to a binded int event successfully", async () => {
        u.getAsync("/q//Unium.UniumHelpers.InvokeTestIntEvent()");

        const response = await u.waitForEventAsync("TestIntEvent", 3000);
        expect(response.id).toEqual("TestIntEvent");
        expect(response.data).toBe(1);
    });
})