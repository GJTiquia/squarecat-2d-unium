import { UniumWebSocket } from "../../src/UniumWebSocket";
import { PRODUCT_NAME, COMPANY_NAME, INITAL_SCENE } from "../../src/config";

describe("WebSocket Get Test", () => {
    let u: UniumWebSocket;

    beforeAll(async () => {
        u = new UniumWebSocket();
        await u.connectAsync();
    })

    afterAll(async () => {
        await u.disconnectAsync();
    })

    it("should be able to get the about correctly", async () => {
        const about = await u.getAsync("/about");
        expect(about).toBeDefined();
        expect(about.Product).toEqual(PRODUCT_NAME);
        expect(about.Company).toEqual(COMPANY_NAME);
        expect(about.Scene).toEqual(INITAL_SCENE);
    })

    it("should return an array of game objects with length of 1 when looking for DeveloperTools", async () => {
        const response = await u.getAsync("/q/scene//DeveloperTools");
        expect(response).toBeDefined();
        expect(response).toBeInstanceOf(Array);
        expect(response).toHaveLength(1);
    })

    it("should return an array of game objects with length of 0 when looking for a GameObject that does not exist", async () => {
        const response = await u.getAsync("/q/scene//SomethingThatDoesNotExist");
        expect(response).toBeDefined();
        expect(response).toBeInstanceOf(Array);
        expect(response).toHaveLength(0);
    })

    it("should be able to get the position of DeveloperTools correctly as { x: 0, y: 0, z: 0 }", async () => {
        const responseArray = await u.getAsync("/q/scene//DeveloperTools.Transform.position");
        expect(responseArray[0]).toEqual({ x: 0, y: 0, z: 0 })
    })
})