import { UniumWebSocket } from "../../src/UniumWebSocket";
import { COMPANY_NAME, INITAL_SCENE, PRODUCT_NAME } from "../../src/config";
import { About } from "../../src/types";

describe("WebSocket Connection Test", () => {
    let u: UniumWebSocket;
    let about: About;

    beforeAll(async () => {
        u = new UniumWebSocket();
        await u.connectAsync();

        about = await u.getAboutAsync();
    })

    afterAll(async () => {
        await u.disconnectAsync();
    })

    it(`should have ${PRODUCT_NAME} as the Product name`, async () => {
        expect(about.Product).toEqual(PRODUCT_NAME)
    })

    it(`should have ${COMPANY_NAME} as the Company name`, async () => {
        expect(about.Company).toEqual(COMPANY_NAME)
    })

    it(`should have ${INITAL_SCENE} as the initial scene`, async () => {
        expect(about.Scene).toEqual(INITAL_SCENE)
    })
})