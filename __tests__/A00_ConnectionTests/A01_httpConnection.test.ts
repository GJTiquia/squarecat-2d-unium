import { COMPANY_NAME, INITAL_SCENE, PRODUCT_NAME, url } from "../../src/config"
import { About } from "../../src/types";

describe("HTTP Connection Test", () => {
    let response: Response;
    let about: About;

    beforeAll(async () => {
        response = await fetch(`${url}/about`);
        about = await response.json();
    })

    it("should return status code 200", async () => {
        expect(response.status).toBe(200);
    })

    it(`should have ${PRODUCT_NAME} as the Product name`, async () => {
        expect(about.Product).toEqual(PRODUCT_NAME)
    })

    it(`should have ${COMPANY_NAME} as the Company name`, async () => {
        expect(about.Company).toEqual(COMPANY_NAME)
    })

    it(`should have ${INITAL_SCENE} as the initial scene`, async () => {
        expect(about.Scene).toEqual(INITAL_SCENE)
    })
})