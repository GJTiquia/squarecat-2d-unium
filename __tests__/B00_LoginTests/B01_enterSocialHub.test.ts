import { UniumWebSocket } from "../../src/UniumWebSocket";
import { url } from "../../src/config"
import { SceneName } from "../../src/enums";
import { About } from "../../src/types";
import { sleep } from "../../src/utils";

describe("Log In to Social Hub Test", () => {
    let u: UniumWebSocket;

    beforeAll(async () => {
        u = new UniumWebSocket();
        await u.connectAsync();
    })

    afterAll(async () => {
        await u.disconnectAsync();
    })

    it("should be in UIOnlyScene", async () => {
        const about = await u.getAboutAsync();
        expect(about.Scene).toEqual(SceneName.UIOnlyScene);
    })

    it("should get into SocialHubScene from SplashView within 20 seconds", async () => {
        expect.assertions(1);

        await sleep(500); // Short buffer to prevent triggering speed hack
        await u.getAsync("/q/scene//Unium.UniumHelpers.StartGame()");

        const isSceneChanged = await u.waitForScene(SceneName.SocialHubScene);
        expect(isSceneChanged).toBeTruthy();
    }, 20000)
})