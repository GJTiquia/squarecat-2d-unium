import { createTestModel } from "@xstate/test";
import { MapName, SmokeTestMachine } from "../../src/machines/SmokeTestMachine";
import { UniumWebSocket } from "../../src/UniumWebSocket";
import { SceneName } from "../../src/enums";
import { sleep } from "../../src/utils";
import { Vector3 } from "../../src/types";
import { expectVector3ToBeClose } from "../../src/utils/testUtils";
import { assign } from "xstate";

const machine = SmokeTestMachine
    .withContext({
        mapName: "NONE"
    })
    .withConfig(
        {
            actions: {
                SetMapName: assign((context, event) => {
                    if (event.type === "TELEPORT_TO_MAP_TRIGGER")
                        return { mapName: event.targetMapName }

                    return {}
                })
            }
        }
    );


const model = createTestModel(machine, {
    // https://github.com/statelyai/xstate/blob/main/packages/xstate-test/CHANGELOG.md#100-alpha1
    // https://github.com/statelyai/xstate/pull/3864
    events: [
        { type: "TELEPORT_TO_MAP_TRIGGER", targetMapName: "Map 1" },
        { type: "TELEPORT_TO_MAP_TRIGGER", targetMapName: "Map 2" },
    ]
});

// TODO : Refactor!
async function getMapTriggerPosition(u: UniumWebSocket, mapName: MapName): Promise<Vector3> {
    let mapTriggerPosition: Vector3 | undefined;

    if (mapName === "Map 1")
        mapTriggerPosition = await u.getFirstResultAsync("/q/scene//Map1TriggerTeleportPoint.Transform.position")

    else if (mapName === "Map 2")
        mapTriggerPosition = await u.getFirstResultAsync("/q/scene//Map2TriggerTeleportPoint.Transform.position")

    if (!mapTriggerPosition)
        throw new Error(`getMapTriggerPosition: Did not handle the case for map name: ${mapName}!`)

    return mapTriggerPosition;
}


describe("XState Smoke Test", () => {

    let u: UniumWebSocket;

    beforeAll(async () => {
        u = new UniumWebSocket();
        await u.connectAsync();
    })

    afterAll(async () => {
        await u.disconnectAsync();
    })

    model.getShortestPaths().forEach(path => {

        it(path.description, async () => {

            await path.test({

                // States should be able to instantly check at that moment if they are in the right state
                // The initial state should be reachable by other states, so no need to restart the Unity Editor.
                states: {
                    "InSocialHubScene": async (state) => {
                        const sceneName = await u.getAboutAsync();
                        expect(sceneName.Scene).toEqual(SceneName.SocialHubScene);
                    },

                    "InSocialHubScene.NearSpawnPoint": async (state) => {
                        const spawnPoint: Vector3 = { x: 0, y: 0, z: 0 }
                        const currentPosition: Vector3 = await u.getFirstResultAsync("/q/scene//Unium.UniumHelpers.GetLocalPlayerPosition()")
                        expectVector3ToBeClose(currentPosition, spawnPoint);
                    },

                    "InSocialHubScene.NearMapTrigger": async (state) => {
                        const mapTriggerPosition: Vector3 = await getMapTriggerPosition(u, state.context.mapName);
                        const currentPosition: Vector3 = await u.getFirstResultAsync("/q/scene//Unium.UniumHelpers.GetLocalPlayerPosition()")
                        expectVector3ToBeClose(currentPosition, mapTriggerPosition);
                    },

                    "InGameScene": async (state) => {
                        const sceneName = await u.getAboutAsync();
                        expect(sceneName.Scene).toEqual(SceneName.GameScene);

                        const mapName: string = await u.getFirstResultAsync("/q/scene//Unium.UniumHelpers.GetCurrentMap()")
                        expect(mapName).toEqual(state.context.mapName);
                    },
                },

                // Events can be long-running events, waiting until the event has finished
                events: {
                    TELEPORT_TO_MAP_TRIGGER: async (step) => {
                        const mapTriggerPosition = await getMapTriggerPosition(u, step.event.targetMapName);
                        await sleep(50);
                        await u.getAsync(`/q/scene//Unium.UniumHelpers.TeleportLocalPlayer(${JSON.stringify(mapTriggerPosition)})`);
                    },

                    INTERACT: async (step) => {
                        await u.getAsync("/q/scene//Unium.UniumHelpers.Interact()");
                    },

                    TRIGGER_EXIT_PORTAL: async (step) => {
                        await sleep(50);
                        await u.getAsync("/q/scene//ExitPortal.ExitPortal.ReturnToSocialHub()");

                        const sceneLoaded = await u.waitForScene(SceneName.SocialHubScene);
                        expect(sceneLoaded).toBeTruthy();
                    },
                }
            })
        })
    });
})