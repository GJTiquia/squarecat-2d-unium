# Unium Cheatsheet

## Queries

`/about` : About

`/q/scene//GameObjectName` : Find GameObject in scene (including DontDestroyOnLoad) by name. Returns an array.

## WebSocket Protocol

- Request
  - { id: "message-id", q: "query" }
- Good Response
  - { id: "message-id", data: ...}
- Bad Response
  - { id: "message-id", error: "..."}
