
// This file was automatically generated. Edits will be overwritten

export interface Typegen0 {
    '@@xstate/typegen': true;
    internalEvents: {
        "xstate.init": { type: "xstate.init" };
    };
    invokeSrcNameMap: {

    };
    missingImplementations: {
        actions: "SetMapName";
        delays: never;
        guards: never;
        services: never;
    };
    eventsCausingActions: {
        "SetMapName": "TELEPORT_TO_MAP_TRIGGER";
    };
    eventsCausingDelays: {

    };
    eventsCausingGuards: {

    };
    eventsCausingServices: {

    };
    matchesStates: "InGameScene" | "InSocialHubScene" | "InSocialHubScene.NearMapTrigger" | "InSocialHubScene.NearSpawnPoint" | { "InSocialHubScene"?: "NearMapTrigger" | "NearSpawnPoint"; };
    tags: never;
}
