import * as crypto from "crypto";
import { About } from "./types";
import * as config from "./config";
import { SceneName } from "./enums";

const WebSocket = require('ws');

type EventID = string;
type EventCallback = (response: MessageResponse) => void;
type EventCallbacks = Array<EventCallback>;
type EventMap = Map<EventID, EventCallbacks>;

interface MessageRequest {
    id: string,
    q: string
}

interface MessageResponse {
    id: string,
    data?: any,
    error?: string
}

interface BindResponse extends MessageResponse {
    info: string
}

export class UniumWebSocket {
    private _socket?: WebSocket;
    private _oneShotEvents: EventMap;
    private _events: EventMap;

    constructor() {
        this._oneShotEvents = new Map<EventID, EventCallbacks>();
        this._events = new Map<EventID, EventCallbacks>();
    }

    public connectAsync(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.disconnectAsync();

            this._socket = new WebSocket(config.ws);

            this._socket?.addEventListener("open", () => resolve());
            this._socket?.addEventListener("error", () => reject("UniumWebSocket.connect: Websocket error. Cannot connect!"));
            this._socket?.addEventListener("message", (e) => this.onMessageEventHandler(e));
        })
    }

    public async disconnectAsync(): Promise<void> {
        if (this._socket) {
            this._socket.close();
            this._socket = undefined;
        }
    }

    public async getAboutAsync(): Promise<About> {
        const about = await this.getAsync("/about");
        return about as About;
    }

    public async getAsync(query: string): Promise<any> {
        const msg: MessageRequest = {
            id: crypto.randomUUID(),
            // id: query, // Uncomment this to debug errors if needed
            q: query
        }

        const response = await this.sendOneShotMessageRequest(msg);
        return response.data;
    }

    public async getFirstResultAsync<T>(query: string): Promise<T> {
        const results = await this.getAsync(query);
        if (!(results instanceof Array))
            throw new Error(`Response for query "${query}" is not an Array! Results: ${results}`);

        if (results.length <= 0)
            throw new Error(`No results for query "${query}"!`);

        return results[0] as T;
    }

    // In Unity C# code, remember to add the event keyword and must be Action<object>!!
    public async bindAsync(query: string, id: string): Promise<BindResponse> {
        const msg: MessageRequest = {
            id: id,
            q: `/bind${query}`
        }

        const response = await this.sendMessageRequest(msg) as BindResponse;
        return response;
    }

    public async unbindAsync(id: string): Promise<BindResponse> {
        const msg: MessageRequest = {
            id: id,
            q: `/socket.unbind(${id})`
        }

        const response = await this.sendMessageRequest(msg) as BindResponse;

        // Clear all subscribers
        if (this._events.has(id))
            this._events.get(id)!.length = 0

        return response;
    }

    public waitForEventAsync(messageID: string, timeout: number): Promise<MessageResponse> {
        return new Promise<MessageResponse>((resolve, reject) => {
            const timeoutHandler = setTimeout(
                () => reject(`${messageID}: Timed Out!`),
                timeout
            );

            this.subscribeToEvent(messageID, (messageResponse) => {
                clearTimeout(timeoutHandler);
                resolve(messageResponse);
            })
        });
    }

    public async gameObjectExists(name: string): Promise<boolean> {
        let searchResultArray = await this.getAsync(`/q/scene//${name}`)
        return searchResultArray.length > 0;
    }

    public async waitForGameObjectToExist(name: string): Promise<boolean> {
        let exists = await this.gameObjectExists(name);

        while (!exists)
            exists = await this.gameObjectExists(name);

        return true;
    }

    public async waitForGameObjectToNotExist(name: string): Promise<boolean> {
        let exists = await this.gameObjectExists(name);

        while (exists)
            exists = await this.gameObjectExists(name);

        return true;
    }

    public async waitForScene(sceneName: SceneName): Promise<boolean> {
        let about = await this.getAboutAsync();

        while (about.Scene !== sceneName)
            about = await this.getAboutAsync();

        return true;
    }

    private onMessageEventHandler(e: MessageEvent<any>) {
        const messageResponse: MessageResponse = JSON.parse(e.data);

        if (messageResponse.error)
            throw new Error(`UniumWebSocket.onMessageEventHandler: ${JSON.stringify(messageResponse)}`);

        // Handle OneShotEvents
        if (this._oneShotEvents.has(messageResponse.id)) {
            this._oneShotEvents.get(messageResponse.id)?.forEach(callback => callback(messageResponse));
            this._oneShotEvents.delete(messageResponse.id);
        }

        // Handle Events
        if (this._events.has(messageResponse.id)) {
            this._events.get(messageResponse.id)?.forEach(callback => callback(messageResponse));
        }
    }

    private sendOneShotMessageRequest(messageRequest: MessageRequest, timeout: number = 5000): Promise<MessageResponse> {
        return new Promise<MessageResponse>((resolve, reject) => {
            const timeoutHandler = setTimeout(
                () => reject(`${messageRequest.id}: Timed Out!`),
                timeout
            );

            this._socket?.send(JSON.stringify(messageRequest));

            this.subscribeToOneShotEvent(messageRequest.id, (messageResponse) => {
                clearTimeout(timeoutHandler);
                resolve(messageResponse);
            })
        });
    }

    private subscribeToOneShotEvent(messageID: EventID, callback: EventCallback) {
        if (!this._oneShotEvents.has(messageID)) {
            this._oneShotEvents.set(messageID, []);
        }

        this._oneShotEvents.get(messageID)?.push(callback);
    }

    private sendMessageRequest(messageRequest: MessageRequest, timeout: number = 5000): Promise<MessageResponse> {
        return new Promise<MessageResponse>((resolve, reject) => {
            const timeoutHandler = setTimeout(
                () => reject(`${messageRequest.id}: Timed Out!`),
                timeout
            );

            // console.log("sent", messageRequest);
            this._socket?.send(JSON.stringify(messageRequest));

            this.subscribeToEvent(messageRequest.id, (messageResponse) => {
                clearTimeout(timeoutHandler);
                resolve(messageResponse);
            })
        });
    }

    private subscribeToEvent(messageID: EventID, callback: EventCallback) {
        if (!this._events.has(messageID)) {
            this._events.set(messageID, []);
        }

        this._events.get(messageID)?.push(callback);
    }
}