import { UniumWebSocket } from "./UniumWebSocket";

async function main() {
    try {
        const u = new UniumWebSocket();
        await u.connectAsync();

        const response = await u.getAsync("/q/scene//ExitPortal")
        console.log(response);

        await u.disconnectAsync();
    }
    catch (error) {
        console.error(error);
    }
}

main();
