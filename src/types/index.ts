export interface Vector3 {
    x: number,
    y: number,
    z: number
}

export interface GameObject {
    name: string,
    tag: string,
    activeInHierarchy: string,
    components: string[],
    children: string[]
}

export interface About {
    Product: string,
    Company: string,
    Scene: string
}