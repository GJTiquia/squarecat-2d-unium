import { SceneName } from "./enums"

const host = "127.0.0.1" // localhost doesnt work which is weird. 127.0.0.1 works
const port = "8342"

export const url = `http://${host}:${port}`
export const ws = `ws://${host}:${port}/ws`

export const PRODUCT_NAME = "SquareCat Saga"
export const COMPANY_NAME = "GJTiquia"
export const INITAL_SCENE: SceneName = SceneName.UIOnlyScene;
