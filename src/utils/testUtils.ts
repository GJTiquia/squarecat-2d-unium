import { Vector3 } from "../types";

export function expectVector3ToBeClose(a: Vector3, b: Vector3) {
    expect(a.x).toBeCloseTo(b.x);
    expect(a.y).toBeCloseTo(b.y);
    expect(a.z).toBeCloseTo(b.z);
}