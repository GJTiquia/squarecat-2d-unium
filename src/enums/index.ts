export enum SceneName {
    UIOnlyScene = "UIOnlyScene",
    SocialHubScene = "SocialHubScene",
    GameScene = "GameScene"
}